
class Aluno
{
    public string? Matricula { get; set; }
    public string? Nome { get; set; }
    // {
    //     get
    //     {
    //         return Nome;
    //     }
    //     set
    //     {
    //         if (Nome == null)
    //         {
    //             Nome = value;
    //         }
    //     }
    // }


    public Aluno()
    {
    }
    public Aluno(string NomeParam)
    {
        Nome = NomeParam;
    }

    public Aluno(string NomeParam, string MatriculaParam)
    {
        Nome = NomeParam;
        Matricula = MatriculaParam;
    }


    //public string? Aviao;

    public double Nota1;
    public double Nota2;


    public override string ToString()
    {
        return "Nome: " + Nome + ", Matrícula: " + Matricula;
    }

}