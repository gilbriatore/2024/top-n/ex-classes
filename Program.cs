﻿

class Program
{

    static void Main()
    {

        double res = Calculadora.Somar(10, 5);
        //Aluno.Nome = "João";



        Aluno objAluno1 = new Aluno("João");
        //objAluno1.Nome = "João";


        Aluno objAluno2 = new Aluno("Ana", "321");
        // objAluno1.Nome = "Ana";
        // objAluno1.Matricula = "321";

        Aluno objAluno3 = new Aluno();
        Aluno objAluno4 = new Aluno();

        //objAluno1.Nome = "Qualquer";

        //Console.WriteLine("Nome 1: " + objAluno1.Nome);
        // Console.WriteLine(objAluno1);
        // Console.WriteLine(objAluno2);
        // Console.WriteLine(objAluno3);
        // Console.WriteLine(objAluno4);

        double variavel = 10;
        double[] vetNumeros = { 0, 3, 6 };

        Aluno objeto = new Aluno();

        //Quantos alunos?
        int qtde = 4;

        Aluno[] vetObjetos = new Aluno[qtde];
        vetObjetos[0] = objAluno1;
        vetObjetos[1] = objAluno2;
        vetObjetos[2] = objAluno3;
        vetObjetos[3] = objAluno4;

        //+1 aluno
        Aluno[] vetObjetos2 = new Aluno[5];
        vetObjetos2[0] = vetObjetos[0];
        vetObjetos2[1] = vetObjetos[1];
        vetObjetos2[2] = vetObjetos[2];
        vetObjetos2[3] = vetObjetos[3];
        vetObjetos2[3] = new Aluno();

        // Listas dinâmicas
        List<Aluno> alunos = new List<Aluno>();
        alunos.Add(objAluno1);
        alunos.Add(objAluno2);
        alunos.Add(objAluno3);
        alunos.Add(objAluno4);
        alunos.Add(new Aluno());

        //for(int i = 0; i < 5; i++){ }
        foreach (Aluno aluno in alunos)
        {
            Console.WriteLine(aluno);
        }

    }

    //public static void Main(String[] args)
    static void MainOld()
    {

        Aluno obj1 = new Aluno();
        obj1.Matricula = "A";
        obj1.Nome = "Nome 1";
        obj1.Nota1 = 6;
        obj1.Nota2 = 9;

        Aluno obj2 = new Aluno();
        obj2.Matricula = "B";
        obj2.Nome = "Nome 2";
        obj2.Nota1 = 7;
        obj2.Nota2 = 8;

        Aluno obj3 = new Aluno();
        Aluno obj4 = new Aluno();
        Aluno obj5 = new Aluno();

        double dolar = 0.333333333;

        Console.WriteLine($"Dolar: {String.Format("{0:0.##}", dolar)}");
        Console.WriteLine($"Dolar: {String.Format("{0:0.00}", dolar)}");


    }



    // Declaração da função
    static void Executar()
    {

        Console.WriteLine("Digite a nome do aluno");
        string nome = Console.ReadLine();

        Console.WriteLine("Digite a nota 1");
        double nota1 = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Digite a nota 2");
        double nota2 = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine($"Nome: {nome}");
        Console.WriteLine($"Nota 1: {nota1}");
        Console.WriteLine($"Nota 2: {nota2}");

        Console.WriteLine($"Somar: {Calculadora.Somar(nota1, nota2)}");
        Console.WriteLine($"Subtrair: {Calculadora.Subtrair(nota1, nota2)}");
        Console.WriteLine($"Multiplicar: {Calculadora.Multiplicar(nota1, nota2)}");
        Console.WriteLine($"Dividir: {Calculadora.Dividir(nota1, nota2)}");

    }

    // Invocação da função
    //Executar();


}




